# SPDX-License-Identifier: GPL-3.0-or-later
# frozen_string_literal: true

$:.unshift(__dir__ + "/lib")

require 'oidc_aws'

run OidcAws::App
