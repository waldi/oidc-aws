# SPDX-License-Identifier: GPL-3.0-or-later
# frozen_string_literal: true

require 'aws-sdk-core'
require 'cgi'
require 'haml'

module OidcAws
  module AuthController
    module Helpers
      def lookup_session
        session = @id_token.dig(:email) || @omniauth_auth.dig(:extra, :raw_info, :nickname) || @omniauth_auth.dig(:extra, :raw_info, :email)
        return session if session

        raise Forbidden, 'Unable to find nickname or email address'
      end

      def lookup_account(accounts, account_name)
        accounts.each do |account|
          return account if account.fetch(:name) == account_name
        end
        raise Forbidden, 'Invalid account or provider name'
      end

      def lookup_role(roles, role_name, provider_name)
        roles.each do |role|
          return role if role.fetch(:name) == role_name && role.fetch(:provider) == provider_name
        end
        raise Forbidden, 'Invalid role name'
      end

      def aws_console_signin(credentials, auth, account, role)
        cred_json = {
          sessionId: credentials.access_key_id,
          sessionKey: credentials.secret_access_key,
          sessionToken: credentials.session_token,
        }.to_json
        signin_token_url = URI(
          'https://signin.aws.amazon.com/federation?'\
          'Action=getSigninToken&'\
          'DurationSeconds=900&'\
          "SessionType=json&Session=#{CGI.escape(cred_json)}"
        )
        signin_token = JSON.parse(Net::HTTP.get(signin_token_url)).fetch('SigninToken')
        issuer_url = URI(
          "#{settings.base_url}"\
          "/auth/#{auth[:provider]}?"\
          'action=login&'\
          "account=#{account[:name]}&"\
          "role=#{role[:name]}"
        )
        destination_url = URI(
          'https://console.aws.amazon.com/'
        )

        URI(
          'https://signin.aws.amazon.com/federation?'\
          'Action=login&'\
          "SigninToken=#{CGI.escape(signin_token)}&"\
          "Issuer=#{CGI.escape(issuer_url.to_s)}&"\
          "Destination=#{CGI.escape(destination_url.to_s)}"
        )
      end
    end

    def self.registered(app)
      app.helpers Helpers

      app.before '/auth/:provider/callback' do
        @omniauth_auth = request.env['omniauth.auth']
        @omniauth_params = request.env.fetch('omniauth.params', {})

        id_token_raw = @omniauth_auth.dig(:credentials, :id_token)
        # XXX: Can we check the token?
        @id_token = JSON::JWT.decode(id_token_raw, :skip_verification)

        @session = lookup_session

        account_name = @omniauth_params.fetch('account')
        @account = lookup_account(settings.accounts, account_name)

        role_name = @omniauth_params.fetch('role')
        @role = lookup_role(@account.fetch(:roles), role_name, params[:provider])

        group_names = @id_token.dig(:groups) || @omniauth_auth.dig(:extra, :raw_info, :groups) || []
        @groups = (@role.fetch(:groups) & group_names)
        if @groups.empty?
          raise AuthenticationFailed, "Unauthorized login for \"#{@session}\" to account \"#{@account[:description]}\" role \"#{@role[:description]}\""
        end

        client = Aws::STS::Client.new(
          region: 'aws-global',
          credentials: false,
        )
        @role_arn = @role.fetch(:aws_arn)
        begin
          @credentials = client.assume_role_with_web_identity(
            duration_seconds: 900,
            role_arn: @role_arn,
            role_session_name: @session,
            web_identity_token: id_token_raw,
          ).credentials
        rescue Aws::STS::Errors::AccessDenied
          raise AuthenticationFailed, "AWS refused login for \"#{@session}\" to account \"#{@account[:description]}\" role \"#{@role[:description]}\""
        rescue Aws::STS::Errors::InvalidIdentityToken => e
          raise AuthenticationFailed, "AWS unable to check user info: #{e.message}"
        end

        logger.info "Authentication successful for #{@session} to #{@role_arn}"
      end

      app.set(:action) { |value| condition { @omniauth_params['action'] == value } }

      app.get '/auth/:provider/callback', action: 'login' do
        redirect aws_console_signin(@credentials, @omniauth_auth, @account, @role)
      end

      app.get '/auth/:provider/callback', action: 'debug' do
        raise Forbidden, 'Not running in development mode' unless settings.environment == :development

        @signin_url = aws_console_signin(@credentials, @omniauth_auth, @account, @role)
        haml :auth_callback_debug
      end

      app.get '/auth/:provider/callback', action: 'show' do
        haml :auth_callback_credentials
      end
    end
  end
end
