# SPDX-License-Identifier: GPL-3.0-or-later
# frozen_string_literal: true

require 'haml'
require 'omniauth'
require 'omniauth_openid_connect'
require 'sinatra'
require 'sinatra/config_file'
require 'thin'

require 'oidc_aws/auth'

module OidcAws
  class AuthenticationFailed < RuntimeError
    def http_status
      200
    end
  end

  class Forbidden < RuntimeError
    def http_status
      403
    end
  end

  class App < Sinatra::Base
    register Sinatra::ConfigFile

    config_file '../config.yml'

    set :protection,
        use: %i[
          content_security_policy
          referrer_policy
          strict_transport
        ],
        frame_options: :deny,
        max_age: 2_592_000,
        img_src: "'none'",
        font_src: "'none'",
        style_src: "'self'",
        default_src: "'none'"

    set :sessions,
        httponly: true

    configure do
      enable :logging
      set :server, 'thin'
    end

    set :public_folder, (proc { File.join(__dir__, '../public') })
    set :views, (proc { File.join(__dir__, '../views') })

    settings.providers.each do |i|
      base_url = settings.base_url
      name = i.fetch(:name)
      use OmniAuth::Strategies::OpenIDConnect, {
        name: name,
        scope: [:openid],
        response_type: :code,
        issuer: i.fetch(:issuer),
        discovery: true,
        client_options: {
          identifier: i.fetch(:identifier),
          secret: i.fetch(:secret),
          redirect_uri: "#{base_url}/auth/#{name}/callback",
        },
      }
    end

    error AuthenticationFailed do
      @error = env['sinatra.error'].message
      logger.info "Authentication failed: #{@error}"
      haml :index
    end

    error Forbidden do
      @error = env['sinatra.error'].message
      logger.info "Authentication failed: #{@error}"
      haml :error
    end

    get '/' do
      haml :index
    end

    register AuthController
  end
end
